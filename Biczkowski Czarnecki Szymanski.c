/**
* \file ts_y.c
* \brief Projekt zaliczeniowy Teoria Sterowania LAB
*/


/**
* \mainpage
* Projekt zaliczeniowy Teoria Sterowania LAB
* \author Autorzy: Kajetan Biczkowski, Maciej Czarnecki, Michal Szymanski
* \date 2014.06.09
* \version 1.0
* \par Kontakt:
* \a maciej.czarnecki@student.put.poznan.pl
*/

///library to operation with servos
#include <Servo.h>

/// Arduino pins for the shift register
#define MOTORLATCH 12 
#define MOTORCLK 4 
#define MOTORENABLE 7 
#define MOTORDATA 8


/**8-bit bus after the 74HC595 shift register 
*(not Arduino pins)
*These are used to set the direction of the bridge driver.
*/
#define MOTOR1_A 7
#define MOTOR1_B 5


/// Arduino pins for the PWM signals.
#define MOTOR1_PWM 11

/// Codes for the motor function.

#define FORWARD 1
#define BACKWARD 2
#define BRAKE 3
#define RELEASE 4

///Declare classes for Servo connectors of the MotorShield.
Servo myservo;

/// Servos position
int pos = 90;
int a=0;

///initialization
void setup()
{
/// Servo on PIN 10
  myservo.attach(10);
}

///main function execution indefinitely
void loop()
{
///single execution
  while(a<1)
  {
  a=1;

///servo 90st
  myservo.write(90);

  delay(500);

///riding in front of a full engine power
  motor(FORWARD, 255);

  delay(1000);

///turn right
  for(pos = 90; pos < 130; pos += 1) // goes from 90 degrees to 130 degrees
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(5);                        // waits 5ms for the servo to reach the position 
  } 
  delay(5000);
  
///riding in front
  for(pos = 130; pos > 90; pos -= 1)
  {                                  
    myservo.write(pos);               
    delay(5);                       
  }
   
  delay(2000); 

///turn left
  for(pos = 90; pos>=60; pos-=1)      
  {                                
    myservo.write(pos);               
    delay(5);                       
  } 
  delay(5000);
  pos=90;
  myservo.write(pos); 
  delay(1000);    
  
  motor(RELEASE, 0);
  }
}

///motor configuration
void motor(int command, int speed)
{
  int motorA, motorB;

  motorA   = MOTOR1_A;
  motorB   = MOTOR1_B;

  switch (command)
  {
  case FORWARD:
    motor_output (motorA, HIGH, speed);
    motor_output (motorB, LOW, -1);     // -1: no PWM set
    break;
  case BACKWARD:
    motor_output (motorA, LOW, speed);
    motor_output (motorB, HIGH, -1);    // -1: no PWM set
    break;
  case BRAKE:
  
    motor_output (motorA, LOW, 255); // 255: fully on.
    motor_output (motorB, LOW, -1);  // -1: no PWM set
    break;
  case RELEASE:
    motor_output (motorA, LOW, 0);  // 0: output floating.
    motor_output (motorB, LOW, -1); // -1: no PWM set
    break;
  default:
    break;
  }

}


// ---------------------------------
// motor_output
//
// The function motor_ouput uses the motor driver to
// drive normal outputs like lights, relays, solenoids, 
// DC motors (but not in reverse).
//
// It is also used as an internal helper function 
// for the motor() function.
//
// The high_low variable should be set 'HIGH' 
// to drive lights, etc.
// It can be set 'LOW', to switch it off, 
// but also a 'speed' of 0 will switch it off.
//
// The 'speed' sets the PWM for 0...255, and is for 
// both pins of the motor output.
//   For example, if motor 3 side 'A' is used to for a
//   dimmed light at 50% (speed is 128), also the 
//   motor 3 side 'B' output will be dimmed for 50%.
// Set to 0 for completelty off (high impedance).
// Set to 255 for fully on.
// Special settings for the PWM speed:
//    Set to -1 for not setting the PWM at all.

///motor control
void motor_output (int output, int high_low, int speed)
{
  int motorPWM;

  switch (output)
  {
  case MOTOR1_A:
  case MOTOR1_B:
    motorPWM = MOTOR1_PWM;
    break;

  default:
    // Use speed as error flag, -3333 = invalid output.
    speed = -3333;
    break;
  }

  if (speed != -3333)
  {
    // Set the direction with the shift register 
    // on the MotorShield, even if the speed = -1.
    // In that case the direction will be set, but
    // not the PWM.
    
    shiftWrite(output, high_low);

    // set PWM only if it is valid
    if (speed >= 0 && speed <= 255)    
    {
      analogWrite(motorPWM, speed);
    }
  }
}


// ---------------------------------
// shiftWrite
//
// The parameters are just like digitalWrite().
//
// The output is the pin 0...7 (the pin behind 
// the shift register).
// The second parameter is HIGH or LOW.
//
// There is no initialization function.
// Initialization is automatically done at the first
// time it is used.

///shift register
void shiftWrite(int output, int high_low)
{
  static int latch_copy;
  static int shift_register_initialized = false;

  // Do the initialization on the fly, 
  // at the first time it is used.
  if (!shift_register_initialized)
  {
    // Set pins for shift register to output
    pinMode(MOTORLATCH, OUTPUT);
    pinMode(MOTORENABLE, OUTPUT);
    pinMode(MOTORDATA, OUTPUT);
    pinMode(MOTORCLK, OUTPUT);

    // Set pins for shift register to default value (low);
    digitalWrite(MOTORDATA, LOW);
    digitalWrite(MOTORLATCH, LOW);
    digitalWrite(MOTORCLK, LOW);
    // Enable the shift register, set Enable pin Low.
    digitalWrite(MOTORENABLE, LOW);

    // start with all outputs (of the shift register) low
    latch_copy = 0;

    shift_register_initialized = true;
  }

  // The defines HIGH and LOW are 1 and 0.
  // So this is valid.
  bitWrite(latch_copy, output, high_low);

  // Use the default Arduino 'shiftOut()' function to
  // shift the bits with the MOTORCLK as clock pulse.
  // The 74HC595 shiftregister wants the MSB first.
  // After that, generate a latch pulse with MOTORLATCH.
  shiftOut(MOTORDATA, MOTORCLK, MSBFIRST, latch_copy);
  delayMicroseconds(5);    // For safety, not really needed.
  digitalWrite(MOTORLATCH, HIGH);
  delayMicroseconds(5);    // For safety, not really needed.
  digitalWrite(MOTORLATCH, LOW);
}

